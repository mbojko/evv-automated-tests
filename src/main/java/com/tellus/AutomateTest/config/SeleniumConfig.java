package com.tellus.AutomateTest.config;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumConfig {

	private static WebDriver driver;
	ChromeOptions options = new ChromeOptions();
	public static WebDriverWait waiter;
	
	public SeleniumConfig() {
		options.addArguments("--incognito");
		
	    driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
		waiter = new WebDriverWait(driver, Duration.ofSeconds(3));
		//driver.manage().window().maximize();
	}
	
	static {
		 String chromeDriverPath = System.getProperty("user.dir")+ "/chromedriver.exe";
		 System.setProperty("webdriver.chrome.driver",chromeDriverPath);
	}
	
	public void navigate(String url) {
		driver.navigate().to(url);
	}
	
	public void clickElement(WebElement element) {
		element.click();
	}
	
	public void sendKey(WebElement element, String key) {
		element.sendKeys(key);
	}
	public WebElement locateElementByXPath(String xPath) {
		return driver.findElement(By.xpath(xPath));
	}
	
	public List<WebElement> locateElementsByXPath(String xPath) {
		return driver.findElements(By.xpath(xPath));
	}
	
	public WebElement locateElementByCssSelector(String cssSelector) {
		return driver.findElement(By.cssSelector(cssSelector));
	}
	
	public List<WebElement> locateElementsByCssSelector(String cssSelector) {
		return driver.findElements(By.cssSelector(cssSelector));
	}
	
	public WebElement locateElementById(String id) {
		return driver.findElement(By.id(id));
	}
	
	public List<WebElement> locateElementsById(String id) {
		return driver.findElements(By.id(id));
	}
	
	/**
	 * Use to locate an Element by name attribute
	 * @param name value of the name attribute
	 * @return
	 */
	public WebElement locateElementByname(String name) {
		return driver.findElement(By.name(name));
	}
	
	public List<WebElement> locateElementsByname(String name) {
		return driver.findElements(By.name(name));
	}
	
	public WebElement locateElementByTag(String name) {
		return driver.findElement(By.tagName(name));
	}
	
	public List<WebElement> locateElementsByTag(String name) {
		return driver.findElements(By.tagName(name));
	}
	
	public WebElement locateElementByClassName(String name) {
		return driver.findElement(By.className(name));
	}
	
	public List<WebElement> locateElementsClassName(String name) {
		return driver.findElements(By.className(name));
	}
	
	public WebElement getActiveElement() {
		return driver.switchTo().activeElement();
	}
	
	public void wait(int timeout) {
		try {
			Thread.sleep(timeout * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void pressKey(Keys key) {
		Actions action = new Actions(driver);
		action.sendKeys(key).perform();
	}
	
	public void refreash() {
		driver.navigate().refresh();
	}
	
	public void close() {
		driver.close();
	}
	
	public static String getScreenshotPath(String testName) {
		
		try {
			//This takes a screenshot from the driver and save it to the specified location
	    	 String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
	    	 TakesScreenshot ts = (TakesScreenshot) driver;
	    	 File source = ts.getScreenshotAs(OutputType.FILE);
	    	 String destination = System.getProperty("user.dir") + "/target/screenshots/"+testName+dateName+".png";
	    	 File finalDestination = new File(destination);
	    	 FileUtils.copyFile(source, finalDestination);
	    	 return destination;
		
		} catch (IOException e) {
			System.out.println(" Exception is: "+ e.getMessage() );
		}
		return null; 
}
}
