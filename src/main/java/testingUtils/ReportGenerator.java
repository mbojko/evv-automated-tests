package testingUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.tellus.AutomateTest.config.SeleniumConfig;
import com.aventstack.extentreports.MediaEntityBuilder;

public class ReportGenerator {

	private static ExtentHtmlReporter htmlReporter;
	private static ExtentReports extent;
	private String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm").format(new Date());
	
	public ReportGenerator(String TestType) {
		new File(System.getProperty("user.dir") + "/target/reports").mkdir();
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/target/reports/" + TestType + "-" + timeStamp + ".html");
		extent = new ExtentReports();
        
		extent.attachReporter(htmlReporter);
        htmlReporter.setAppendExisting(true);
        htmlReporter.config().setDocumentTitle("Tellus - eVV Automation");
        htmlReporter.config().setReportName("Tellus - eVV Automation " + TestType + " tests");
        htmlReporter.config().setTheme(Theme.DARK);	
        
	}
	
	public static void reportSuccess(String testName) {
		if(extent !=null) {
			createNewTest(testName).log(Status.PASS, MarkupHelper.createLabel("Test " + testName + " has completed succesfully" , ExtentColor.GREEN));}
	}
	
	public static void reportSkip(String testName) {
		if(extent !=null) {
			createNewTest(testName).log(Status.SKIP, MarkupHelper.createLabel("Test " + testName + " has been skipped" , ExtentColor.YELLOW));
		}
		
	}
	
	public static void reportFail(String testName, String failure) {
		if(extent !=null) {
			String path = SeleniumConfig.getScreenshotPath(testName);
			try {
				createNewTest(testName).log(Status.FAIL, "Test " + testName + " failed with the following: " + failure, MediaEntityBuilder.createScreenCaptureFromPath(path).build());
			} catch (IOException e) {
				
				createNewTest(testName).log(Status.FAIL, "Test " + testName + " failed with the following: " + failure);
			}
		}
	}
	
	private static ExtentTest createNewTest(String testName) {
        ExtentTest extentlog = extent.createTest("Tellus Automation - " + testName, "");
        ExtentTest childextentlog = extentlog.createNode(testName);
        
        return childextentlog;
	}
	
	public static void wrtieReport() {
		extent.flush();
	}
	
}
