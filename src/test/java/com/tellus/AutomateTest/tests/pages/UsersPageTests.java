package com.tellus.AutomateTest.tests.pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import javax.annotation.Nullable;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.tellus.AutomateTest.config.SeleniumConfig;
import com.tellus.AutomateTest.tests.types.Regression;
import com.tellus.AutomateTest.tests.types.Smoke;

public class UsersPageTests extends PageTest {
	  
		@BeforeClass
	    public static void usersPageTestSetup()
	    {
		    config.wait(3);
			config.locateElementByCssSelector("a[href='/users']").click();  
		  
	    }
	    
	    @Category({Smoke.class, Regression.class})
	    @Test
	    public void navigatedToUsersPage()
		{
		 	assertNotNull(config.locateElementByTag("evv-users"));
	    }
	    
	    @Category({Smoke.class, Regression.class})
	    @Test
	    public void addUser() {
	    	WebElement button  = null;
	    	List<WebElement> listOfButtons = config.locateElementByTag("evv-users").findElements(By.tagName("button"));
	    	for(int i = 0; i<listOfButtons.size(); i++) {
	    		if(listOfButtons.get(i).findElement(By.tagName("mat-icon")).getText().contains("add")) {
	    			button = listOfButtons.get(i);
	    			break;
	    		}
	    	}
	    	button.click();
	    	assertNotNull(config.locateElementByTag("evv-invite-user-dialog"));

	    	//First input search
	    	WebElement initalSearch;
	    	initalSearch=config.locateElementByTag("evv-invite-user-dialog").findElement(By.cssSelector("input[placeholder='Search on full matching email and complete phone number']"));
	    	initalSearch.click();
	    	//config.locateElementByTag("evv-invite-user-dialog").findElement(By.cssSelector("input[placeholder='Search on full matching email and complete phone number']")).click();
	    	//email here gets used twice
	    	initalSearch.sendKeys("Ishbeck123@yo.com");
	     
	    	//click search button
	    	getButtonByText("Search", config.locateElementByTag("evv-invite-user-dialog").findElement(By.tagName("mat-dialog-content"))).click();
	    	//Fill in first name field
	    	config.locateElementByname("firstName").sendKeys("Ishbeck");
	    	//Fill in last name field
	    	config.locateElementByname("lastName").sendKeys("AutoTest");
	    	//Fill in user name field
	    	config.locateElementByname("username").sendKeys("Ishbeck");
	    	//Fill in phone number filed
	    	config.locateElementByname("phoneNumber").sendKeys("5615557861");
	    	//Fill in email address
	    	config.locateElementByname("email").sendKeys("Ishbeck123@yo.com");
	    	//Fill in IVR PIN
	    	config.locateElementByname("ivrPin").sendKeys("5643219");
	    	//Select Caregiver Type
	    	config.wait(2);
	    	config.locateElementById("type").click();
	    	config.wait(2);
	    	findOption("ABA, Assistant Behavior Analyst").click();
	    	//Fill in Caregiver DOB
	    	config.locateElementById("dateOfBirth").sendKeys("02/11/1969");
	    	//Fill in rendering provider id
	    	config.locateElementByname("renderingProviderId").sendKeys("960145635");
	    	//Fill in last 4 SSN
	    	config.locateElementByname("ssn").sendKeys("9601");
	    	//Select Roles
	    	config.locateElementByCssSelector("mat-select[aria-label='Roles']").click();
	    	findOption("Admin").click();
	    	config.pressKey(Keys.ESCAPE);
	    	//Fill in Employee ID
	    	config.locateElementByname("employeeId").sendKeys("9601AutoTest");
	    	//Fill in Employee start date
	    	config.locateElementById("employeeStartDate").sendKeys(getDate(null));
	    	//Fill in Employee End Date
	    	config.locateElementById("employeeEndDate").sendKeys(getDate(new Long(2)));
	    	//Fill in Address1
	    	config.locateElementByname("address1").sendKeys("800 Fairway Dr.");
	    	//Fill in Address2
	    	config.locateElementByname("address2").sendKeys("Suite# 300");
	    	//Fill in City
	    	config.locateElementByname("city").sendKeys("Deerfield Beach");
	    	//Select State
	    	config.locateElementByname("state").click();
	    	findOption("Florida").click();
	    	//Fill in Zip Code
	    	config.locateElementByname("zip").sendKeys("33441");
	    	//By.className("mat-button-wrapper");
			//Click Save button
	    	getButtonByText("Invite New User",config.locateElementByTag("form")).click();
	    	config.wait(2);
	    	//Close the search pop up box
	    	SeleniumConfig.waiter.until(ExpectedConditions.elementToBeClickable(config.locateElementByTag("evv-invite-user-dialog").findElement(By.tagName("mat-icon")))).click();
	    	config.wait(1);
	    	
	    	searchForUser("Ishbeck");
	    	assertEquals("Ishbeck", getSearchRecord("Ishbeck"));
	    }
	    
//	    @Category({Smoke.class, Regression.class})
//	    @Test
//	    public void searchForNewlyCreatedUser() {
//	    	
//		    searchForUser("Ishbeck");
//	    	assertEquals("Ishbeck", getSearchRecord("Ishbeck"));
//	    }
//	    
	    private void searchForUser(String user) {
	    	WebElement searchButton = config.locateElementByTag("evv-search-box").findElement(By.tagName("mat-icon"));
	    	//click on magnifying icon
	    	if(searchButton.getText().equalsIgnoreCase("close")) {
	    		searchButton.click();
	    		config.wait(1);
	    	}
	    	searchButton.click();
	    	config.wait(1);
	    	//click on search field
	    	config.locateElementByTag("evv-search-input").findElement(By.cssSelector("input[placeholder = 'Search User']")).click();
	    	config.wait(1);
	    	//Type in the search field for user
	    	config.locateElementByTag("evv-search-input").findElement(By.cssSelector("input[placeholder = 'Search User']")).sendKeys(user);
	    	config.wait(2);
	    	config.pressKey(Keys.ENTER);
	    	config.wait(2);
	    }
	    
	    private static String getSearchRecord(String userName) {
	    	List<WebElement> matCells = config.locateElementsByTag("mat-cell");
	    	String name = matCells.stream()
	    			.map(row -> row.findElement(By.tagName("span")))
	    			.filter(select ->{
	    				return select.getText().trim().contains(userName.trim());
	    			})
	    			.map(filteredRow -> filteredRow.getText().trim())
	    			.findAny()
	    			.orElse(null);
	    	assertNotNull(name);
			return name;
	    }
	    
		private WebElement findOption(String filter) {
			config.wait(1);
			List<WebElement> matOptions = config.locateElementsByTag("mat-option");
			WebElement option = matOptions.stream()
					.filter(select -> {
						WebElement span = select.findElement(By.tagName("span"));
						return span.getText().trim().contains(filter.trim());
					})
					.findAny()
					.orElse(null);
			
			assertNotNull(option);
			return option;
		}
		
		private String getDate(Long yearOffset) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			LocalDate localDate = LocalDate.now();
			if (yearOffset != null) {
				localDate = localDate.plusYears(yearOffset);
			}
			return dtf.format(localDate);
		}

		private WebElement getButtonByText(String buttonText, @Nullable WebElement parent) {
			List<WebElement> buttons = parent == null ? config.locateElementsByTag("button") : parent.findElements(By.tagName("button"));
			
			WebElement buttonFound = buttons.stream()
					.filter(button -> {
						return button.findElement(By.className("mat-button-wrapper")).getText().equals(buttonText);
					})
					.findAny()
					.orElse(null);
			
			assertNotNull(buttonFound);
			return buttonFound;
		}
}
