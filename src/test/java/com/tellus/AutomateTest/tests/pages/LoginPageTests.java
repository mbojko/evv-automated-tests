package com.tellus.AutomateTest.tests.pages;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebElement;

import com.tellus.AutomateTest.config.SeleniumConfig;
import com.tellus.AutomateTest.tests.types.Regression;
import com.tellus.AutomateTest.tests.types.Smoke;

/**
 * Unit test for simple App.
 */
public class LoginPageTests extends PageTest
{

    @Category({Smoke.class,Regression.class})
    @Test
    public void shouldLogIn()
    {      
        assertNotNull(config.locateElementByTag("evv-main"));
    }
}
