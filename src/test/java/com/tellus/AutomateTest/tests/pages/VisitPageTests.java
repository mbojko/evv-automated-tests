package com.tellus.AutomateTest.tests.pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.tellus.AutomateTest.tests.types.Regression;
import com.tellus.AutomateTest.tests.types.Smoke;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VisitPageTests extends PageTest {
	
	@BeforeClass
    public static void visitPageTestsSetup() 
	{
		config.locateElementByCssSelector("a[href$='/visits']").click();
	}
	
	@Category({Smoke.class, Regression.class})
	@Test
	public void Test1_navigatedToVisitsPage()
		{
		 	assertNotNull(config.locateElementByTag("evv-visits"));
	    }
	
	@Category({Smoke.class, Regression.class})
	@Test
	public void Test2_addVisit() {
		config.locateElementByCssSelector("a[href$='/visits/add'").click();
		
		assertNotNull(config.locateElementByTag("evv-visit-add"));
		
		//select recipient
		config.locateElementByCssSelector("input[aria-label='Select Recipient']").sendKeys("paco taco");
		config.wait(3);
		config.locateElementsByTag("mat-option").get(0).click();
		
		//select caregiver
		config.locateElementByCssSelector("input[aria-label='Select Caregiver']").sendKeys("Maria Bojko");
		config.wait(1);
		findOption("Maria Bojko").click();
		config.wait(2);
		
		//select Services
		List<WebElement> services = config.locateElementByTag("evv-select-services").findElements(By.tagName("evv-service"));
		
		WebElement checkbox = services.stream()
								.map(service -> service.findElement(By.tagName("mat-checkbox"))
								)
								.filter(element -> {
									WebElement label = element.findElement(By.className("mat-checkbox-label"));
									
									List<WebElement> spans = label.findElements(By.tagName("span"));
									
									return spans.stream()
											.filter(span -> span.getText().contains("T1030"))
											.collect(Collectors.toList()).size() > 0;
								})
								.map(matBox -> matBox.findElement(By.className("mat-checkbox-inner-container")))
								.findAny()
								.orElse(null);
		
		assertNotNull(checkbox);
		System.out.print(checkbox);
		checkbox.click();
		
		//select Start address
		WebElement startAddress = getAddressElement(0);
		startAddress.click();
		
		//select EndAddress
		WebElement endAddress = getAddressElement(1);
		endAddress.click();
		
		//Select Duration Hours/Minutes
//		config.locateElementByTag("evv-select-date-time").findElements(By.tagName("mat-Select")).get(1).click();
//		config.wait(1);
//		List<WebElement> matOptions = config.locateElementsByTag("mat-option");
//		WebElement setTime = null;
//		
//		for(int i = 0; i < matOptions.size(); i++) {
//			if(matOptions.get(i).findElement(By.tagName("span")).getText().trim().contains("30")) {
//				setTime = matOptions.get(i);
//				break;
//			}
//		}
//		assertNotNull(setTime);
//		setTime.click();
		
		WebElement dateTime = config.locateElementByTag("evv-select-date-time");
		
		getMatSelectByLabel(dateTime, "Duration minutes").click();
		findOption("30").click();
		config.wait(1);
		
		//Click on Save button
		config.locateElementByCssSelector("button[type = 'submit']").click();
		config.wait(3);
		assertNotNull(config.locateElementByTag("evv-visits"));
		config.wait(3);
	}
	
//	@Category({Smoke.class, Regression.class})
//	@Test
//	public void Test3_verifyCreatedVisit() {
//		assertEquals("Maria Bojko", getSearchRecord("Maria Bojko"));
//		//click on snowman
//		config.locateElementByTag("mat-row").findElement(By.cssSelector("span[text ='more_vert']")).click();
//		
//	}
	
	
	private WebElement getAddressElement(int index) {
		WebElement startAddress = config.locateElementsByTag("evv-address").get(index).findElement(By.tagName("evv-select-address"));
		startAddress.findElement(By.tagName("mat-select")).click();
		
		WebElement address = findOption("800 Fairway Drive");
		
		return address;
	}
	
	private WebElement findOption(String filter) {
		config.wait(1);
		List<WebElement> matOptions = config.locateElementsByTag("mat-option");
		WebElement option = matOptions.stream()
				.filter(select -> {
					WebElement span = select.findElement(By.tagName("span"));
					return span.getText().trim().contains(filter.trim());
				})
				.findAny()
				.orElse(null);
		
		assertNotNull(option);
		
		return option;
	}
	
	private WebElement getMatSelectByLabel(WebElement parent, String label) {
		List<WebElement> matForms = parent.findElements(By.tagName("mat-form-field"));
		WebElement matSelect = matForms.stream()
				.filter(form -> {
					WebElement placeholder = form.findElement(By.tagName("mat-placeholder"));
					return placeholder.getText().trim().contains(label.trim());
				})
				.map(select -> select.findElement(By.tagName("mat-select")))
				.findAny()
				.orElse(null);
		
		assertNotNull(matSelect);
		
		return matSelect;
	}
	private static String getSearchRecord(String user) {
    	List<WebElement> matCells = config.locateElementsByTag("mat-cell");
    	String name = matCells.stream()
    			.map(row -> row.findElement(By.tagName("span")))
    			.filter(select ->{
    				return select.getText().trim().contains(user.trim());
    			})
    			.map(filteredRow -> filteredRow.getText().trim())
    			.findAny()
    			.orElse(null);
    	assertNotNull(name);
		return name;
    }
	
}
