package com.tellus.AutomateTest.tests.pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.annotation.Nullable;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;

import com.tellus.AutomateTest.tests.types.Regression;
import com.tellus.AutomateTest.tests.types.Smoke;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RecipientsPageTest extends PageTest {
	  
	@BeforeClass
    public static void recipientsPageTestSetup()
    {
		config.locateElementByCssSelector("a[href$='/recipients']").click();  
	  
    }
		
	@Category({Smoke.class, Regression.class})
    @Test
    public void Test1_navigatedToRecipientPage()
	{
	 	assertNotNull(config.locateElementByTag("evv-recipients-default"));
    }
	
	@Category({Smoke.class, Regression.class})
	@Test
	public void Test2_testManualAddRecipient() {
		config.wait(3);
		config.locateElementByCssSelector("a[href$=\"/recipients/add\"]").click();
		//config.locateElementByTag("section").click();
    	assertNotNull(config.locateElementByTag("evv-recipient-add"));
    	config.wait(3);
    	//click on the Next button
    	config.locateElementByCssSelector("div[class*='push-top']").findElement(By.cssSelector("button[type = 'submit']")).click();
    	config.wait(3);
    	//type first name
    	config.locateElementByname("firstName").sendKeys("Sirene");
    	//type last name
    	config.locateElementByname("lastName").sendKeys("Automate");
    	//type SSN
    	config.locateElementByname("ssn").sendKeys("4532");
    	//type medicaid id
    	config.locateElementByTag("evv-recipient-info-edit").findElement(By.name("medicaidId")).sendKeys("5667586754328");
    	//config.locateElementByname("medicaidId").sendKeys("5667586754328");
    	//click on gender female radio button
    	getRadioButtonByText("Female").click();
    	//type date of birth
    	config.locateElementByTag("evv-recipient-info-edit").findElement(By.name("dateOfBirth")).sendKeys("11/05/1990");
    	//click on Next button
    	clickVisibleNextButton();
    	//type emergency contact name
    	config.locateElementByname("contactName").sendKeys("Maria Healer");
    	//type emergenc contact phone number
    	config.locateElementByname("phoneNumber").sendKeys("5615555551");
    	//select relationship
    	config.locateElementByCssSelector("mat-select[formcontrolname='relationship']").click();
    	findOption(" Sibling ").click();
    	//Click Next button
    	clickVisibleNextButton();
    	//Type find address
    	config.locateElementByCssSelector("input[placeholder='Enter a location']").sendKeys("800 Fairway Drive Deerfield");
    	config.wait(2);
    	config.pressKey(Keys.ARROW_DOWN);
    	config.pressKey(Keys.ENTER);
    	//Select Address Type
    	config.locateElementByCssSelector("mat-select[formcontrolname='addressType']").click();
    	findOption("Residence").click();
    	//Type phone number 1
    	config.locateElementByCssSelector("input[formcontrolname='phone1']").sendKeys("5615555555");
    	//click on Next button
    	clickVisibleNextButton();
    	//select payer id
    	config.locateElementByCssSelector("mat-select[formcontrolname='payerId']").click();
    	findOption("Aetna of Florida").click();
    	//select plan
    	config.locateElementByCssSelector("input[formcontrolname='plan']").click();
    	findOption("Plan: FMSP").click();
    	//type case number
    	config.locateElementByCssSelector("input[formcontrolname='caseNumber']").sendKeys("12345");
    	//type member id
    	config.locateElementByCssSelector("input[formcontrolname='memberId']").sendKeys("5667586754328");
    	//type payerEligibilityEffectiveDate
    	config.locateElementByCssSelector("input[formcontrolname='payerEligibilityEffectiveDate']").sendKeys("09/01/2019");
    	// type payerEligibilityEndDate
    	config.locateElementByCssSelector("input[formcontrolname='payerEligibilityEndDate']").sendKeys("09/30/2020");
    	//select dignosis
    	config.locateElementByCssSelector("input[name='diagnosisCodes']").click();
    	findOption("Localized salmonella infection, unspecified").click();
    	//final save button
    	clickVisibleNextButton();
    	config.wait(7);
    	//navigate back to recipient page
    	config.locateElementByCssSelector("a[href=\'/provider/recipients']").click();
    	
	}
	 @Category({Smoke.class, Regression.class})
	    @Test
	    public void Test3_searchForNewlyCreatedRecipient() {
	    	//click on magnifying icon
	    	config.locateElementByTag("evv-search-box").findElement(By.tagName("mat-icon")).click();
	    	config.wait(1);
	    	//click on search field
	    	config.locateElementByTag("evv-search-input").findElement(By.cssSelector("input[placeholder = 'Search Recipients']")).click();
	    	config.wait(1);
	    	//Type in the search field for user
	    	config.locateElementByTag("evv-search-input").findElement(By.cssSelector("input[placeholder = 'Search Recipients']")).sendKeys("Sirene");
	    	config.wait(2);
	    	config.pressKey(Keys.ENTER);
	    	config.wait(2);
	    	assertEquals("Sirene", getSearchRecord("Sirene"));
	    }
	    
	    private String getSearchRecord(String userName) {
	    	List<WebElement> matCells = config.locateElementsByTag("mat-cell");
	    	String name = matCells.stream()
	    			.map(row -> row.findElement(By.tagName("span")))
	    			.filter(select ->{
	    				return select.getText().trim().contains(userName.trim());
	    			})
	    			.map(filteredRow -> filteredRow.getText().trim())
	    			.findAny()
	    			.orElse(null);
	    	assertNotNull(name);
			return name;
	    }
	
	private void clickVisibleNextButton() {
		config.locateElementByCssSelector("div[class*='mat-horizontal-stepper-content'][style*='visibility: visible']").findElement(By.cssSelector("button[class*='mat-primary']")).click();
	}
	private WebElement getRadioButtonByText(String text) {
		WebElement radioButton = config.locateElementByTag("mat-radio-group").findElements(By.tagName("mat-radio-button")).stream()
			.filter(matRadioButton -> matRadioButton.findElement(By.tagName("label")).getText().equalsIgnoreCase(text))
			.findAny()
			.orElse(null);
		
		assertNotNull(radioButton);
		
		return radioButton;	
	}
	
	private WebElement findOption(String filter) {
		config.wait(1);
		List<WebElement> matOptions = config.locateElementsByTag("mat-option");
		WebElement option = matOptions.stream()
				.filter(select -> {
					WebElement span = select.findElement(By.tagName("span"));
					return span.getText().trim().contains(filter.trim());
				})
				.findAny()
				.orElse(null);
		
		assertNotNull(option);
		return option;
	}
	
	private WebElement getButtonByText(String buttonText, @Nullable WebElement parent) {
		List<WebElement> buttons = parent == null ? config.locateElementsByTag("button") : parent.findElements(By.tagName("button"));
		
		WebElement buttonFound = buttons.stream()
				.filter(button -> {
					return button.findElement(By.className("mat-button-wrapper")).getText().equals(buttonText);
				})
				.findAny()
				.orElse(null);
		
		assertNotNull(buttonFound);
		return buttonFound;
	}
}
