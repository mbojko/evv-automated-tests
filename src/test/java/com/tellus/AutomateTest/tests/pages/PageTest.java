package com.tellus.AutomateTest.tests.pages;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.tellus.AutomateTest.config.SeleniumConfig;

import testingUtils.ReportGenerator;

public class PageTest {
	
	protected static SeleniumConfig config;

    @BeforeClass
    public static void testSetup()
    {
        config = new SeleniumConfig(); 
        config.navigate("https://evv-dashboard.qa.4tellus.net/");
        String elementPath = "username";
        WebElement userName = config.locateElementByname(elementPath);
        config.clickElement(userName);
        config.sendKey(userName, "Automation");
        
        elementPath = "password";
        WebElement password = config.locateElementByname(elementPath);
        config.clickElement(password);
        config.sendKey(password, "Password1!");
        
        elementPath = "button[type='submit']";
        WebElement loginBotton = config.locateElementByCssSelector(elementPath);
        config.clickElement(loginBotton);
        
        changePermissionToProvider();
    }
    
    @AfterClass
    public static void testCleanUp() {
    	config.close();
    }
    
	@Rule
	public TestWatcher watchman= new TestWatcher() {
	     @Override
	     protected void failed(Throwable e, Description description) {
	       ReportGenerator.reportFail(description.getMethodName(), e.getMessage());
	     }

	     @Override
	     protected void succeeded(Description description) {
	    	 ReportGenerator.reportSuccess(description.getMethodName());
	     }
	   };
	   
		private static void changePermissionToProvider() {
			WebElement permissionButton = config.locateElementByTag("evv-select-permission-type");
			String permissionText = permissionButton.findElement(By.tagName("button"))
										.findElement(By.className("mat-button-wrapper"))
										.findElement(By.className("text"))
										.getText();
			
			if (!permissionText.equalsIgnoreCase("Provider")) {
				permissionButton.click();
				
				WebElement providerPermissionButton = config.locateElementByCssSelector("div[class*='mat-menu-panel']")
					.findElements(By.tagName("button")).stream()
					.filter(button -> button.findElement(By.tagName("span")).getText().equalsIgnoreCase("provider"))
					.findAny()
					.orElse(null);

				assertNotNull(providerPermissionButton);
				
				providerPermissionButton.click();
			}		
		}
		
}
