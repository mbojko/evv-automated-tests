package com.tellus.AutomateTest.tests.suites;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import com.tellus.AutomateTest.tests.pages.LoginPageTests;
import com.tellus.AutomateTest.tests.pages.UsersPageTests;
import com.tellus.AutomateTest.tests.pages.VisitPageTests;
import com.tellus.AutomateTest.tests.types.Regression;

import testingUtils.ReportGenerator;

@RunWith(Categories.class)
@IncludeCategory({Regression.class})
@SuiteClasses({LoginPageTests.class, UsersPageTests.class, VisitPageTests.class})
public class RegressionSuite {
	static ReportGenerator report;
	
	@BeforeClass
	public static void setup() {
		report = new ReportGenerator("Critical");
	}
	
	@AfterClass
	public static void tearDown() {
		ReportGenerator.wrtieReport();
	}
}
