package com.tellus.AutomateTest.tests.suites.acha;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import com.tellus.AutomateTest.tests.pages.LoginPageTests;
import com.tellus.AutomateTest.tests.pages.UsersPageTests;
import com.tellus.AutomateTest.tests.pages.VisitPageTests;
import com.tellus.AutomateTest.tests.types.Regression;
import com.tellus.AutomateTest.tests.types.acha.Acha_Regression;

import testingUtils.ReportGenerator;

@RunWith(Categories.class)
@IncludeCategory({Acha_Regression.class})
@SuiteClasses({LoginPageTests.class, UsersPageTests.class, VisitPageTests.class})
public class AchaRegressionSuite {
	static ReportGenerator report;
	
	@BeforeClass
	public static void setup() {
		report = new ReportGenerator("Acha Critical");
	}
	
	@AfterClass
	public static void tearDown() {
		ReportGenerator.wrtieReport();
	}
}
