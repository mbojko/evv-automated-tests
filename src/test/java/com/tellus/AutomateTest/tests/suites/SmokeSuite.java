package com.tellus.AutomateTest.tests.suites;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import com.tellus.AutomateTest.tests.pages.LoginPageTests;
import com.tellus.AutomateTest.tests.pages.RecipientsPageTest;
import com.tellus.AutomateTest.tests.pages.UsersPageTests;
import com.tellus.AutomateTest.tests.pages.VisitPageTests;
import com.tellus.AutomateTest.tests.types.Smoke;

import testingUtils.ReportGenerator;

@RunWith(Categories.class)
@IncludeCategory(Smoke.class)
@SuiteClasses({LoginPageTests.class, UsersPageTests.class, RecipientsPageTest.class, VisitPageTests.class})
public class SmokeSuite{
	
	static ReportGenerator report;
	
	@BeforeClass
	public static void setup() {
		report = new ReportGenerator("Smoke");
	}
	
	@AfterClass
	public static void tearDown() {
		ReportGenerator.wrtieReport();
	}
}
